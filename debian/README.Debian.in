@PACKAGE@ for Debian
--------------------

mail-user-agent
===============

The following configuration enables you to run Mew as a default mail
user agent of emacsen.

	(if (boundp 'mail-user-agent)
	    (setq mail-user-agent 'mew-user-agent))
	(if (boundp 'read-mail-command)
	    (setq read-mail-command 'mew))

Local mailbox
=============

If you want to use a local mailbox instead of POP, you need the
following configuration.

	(setq mew-mailbox-type 'mbox) ;; 'pop is used by default
	(setq mew-mbox-command "incm") ;; incm gets mails from maildir or mbox
	(setq mew-mbox-command-arg "-u -a") ;; See also incm manual

SSL/TLS
=======

To use SSL/TLS, install the ca-certificates package and the stunnel4
package (or the stunnel package).

By default, in the startup file, the mew-prog-ssl variable is set to
"mewstunnel" (wrapper script for using stunnel4 or stunnel), the
mew-ssl-cert-directory variable is set to "/etc/ssl/certs" (directory
to store CA certificates in), and the mew-ssl-verify-level variable
is set to 2 (verify server's certificate).  See also "Mew manual".

	(setq mew-prog-ssl (or (executable-find "mewstunnel") "stunnel"))
	(setq mew-prog-ssl-arg nil)
	;;(setq mew-prog-ssl-arg "ciphers=DEFAULT:@SECLEVEL=2\nsslVersionMin=TLSv1.2\n")
	;;(setq mew-prog-ssl-arg "ciphers=DEFAULT:@SECLEVEL=1\nsslVersionMin=TLSv1.2\nsslVersionMax=TLSv1.2\n")
	;;(require 'gnutls) ;; gnutls-trustfiles
	;;(dolist (file (gnutls-trustfiles)) (setq mew-prog-ssl-arg (concat mew-prog-ssl-arg "CAfile=" file "\n")))
	(setq mew-ssl-cert-directory "/etc/ssl/certs")
	(setq mew-ssl-verify-level 2)

	(setq mew-auto-get nil)
	(setq mew-use-cached-passwd t)
	(setq mew-passwd-timer-unit 60)
	(setq mew-passwd-lifetime 24)
	(setq mew-name "FULL NAME")
	(setq mew-user "USERNAME")
	(setq mew-mail-domain "example.com")
	(setq mew-dcc 'me)
	(setq mew-proto "+")
	;;(setq mew-proto "%")
	(setq mew-smtp-server "smtp.example.com")
	(setq mew-smtp-user "USERNAME@example.com")
	(setq mew-smtp-ssl t)
	;;(setq mew-smtp-port "smtp") ;; 25
	;;(setq mew-smtp-port 2525)
	(setq mew-smtp-port "submission") ;; 587
	(setq mew-smtp-ssl-port mew-smtp-port) ;; STARTTLS
	;;(setq mew-smtp-ssl-port 465)
	(setq mew-pop-server "pop.example.com")
	(setq mew-pop-user "USERNAME@example.com")
	(setq mew-pop-auth 'pass)
	(setq mew-pop-ssl t)
	(setq mew-pop-size 0)
	(setq mew-imap-server "imap.example.com")
	(setq mew-imap-user "USERNAME@example.com")
	(setq mew-imap-ssl t)
	(setq mew-imap-size 0)
	(setq mew-imap-trash-folder "%Trash")
	(setq mew-imap-spam-folder "%Spam")

contrib
=======

contrib/*.el files are installed in the site-lisp directory.

 -- Tatsuya Kinoshita <tats@debian.org>, Wed, 05 May 2021 20:42:40 +0900
